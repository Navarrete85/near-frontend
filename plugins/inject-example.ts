import { Plugin } from '@nuxt/types';

/**
 * Prueba para comprobar como podemos inyectar funciones globales en
 * diferentes contextos, además de la declaración para los diferentes
 * contextos de aplicación, tenemos que registrar el plugin en el archivo
 * nuxt.config.js propiedad plugins...
 * También podemos declarar pligins a nivel de cierto contexto, ejemplos
 * obtenidos de -->
 * https://typescript.nuxtjs.org/es/cookbook/plugins.html#i-inyectar-en-las-instancias-de-vue
 *
 */

/**
 * Inyección en contexto de Vue
 */
declare module 'vue/types/vue' {
  interface Vue {
    $myInjectedFunction(message: string): void;
  }
}

/**
 * Inyección en el contexto de AsyncData "(context)"
 */
declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $myInjectedFunction(message: string): void;
  }
}

/**
 * Inyección en el contexto de store.
 */
declare module 'vuex/types/index' {
  interface Store<S> {
    $myInjectedFunction(message: string): void;
  }
}

const myPlugin: Plugin = (context, inject) => {
  inject('myInjectedFunction', (message: string) => console.log(message));
};

export default myPlugin;
