import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators';
import { $axios } from '~/utils/api';
import { IUser } from '~/common_resources/interfaces/IUser';
import { storeFunction } from '@/utils/utils';

const prefix = 'user/';

export enum UserActions {
  GET_ALL_USERS,
}

export const userActions: { [index in UserActions]: any } = {
  [UserActions.GET_ALL_USERS]: `${prefix}getAllUsers`,
};

@Module({
  name: 'user',
  stateFactory: true,
  namespaced: true,
})
export default class UserModule extends VuexModule {
  users: IUser[] = [];
  user: IUser | undefined;

  @Mutation
  setUsers(users: IUser[]) {
    this.users = users;
  }

  @Mutation
  setUser(user: IUser) {
    this.user = user;
  }

  @Action
  async [`${storeFunction(userActions[UserActions.GET_ALL_USERS])}`]() {
    try {
      const users = await $axios.$get('api/user/all');
      this.setUsers(users);
    } catch (e) {
      console.error('Error al realizar la petición --> ', e);
    }
  }
}
