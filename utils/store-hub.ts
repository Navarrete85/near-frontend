import { userActions, UserActions } from '@/store/user';

const ENUMS = {
  UserActions,
};

const ACTIONS = {
  userActions,
};

export { ENUMS, ACTIONS };
